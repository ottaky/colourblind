chrome.contextMenus.onClicked.addListener((info, tab) => {
  let port = chrome.tabs.connect(tab.id, { name: "cbport" })
  port.postMessage({ "matrix": info.menuItemId })
})

chrome.runtime.onInstalled.addListener(() => {
  chrome.contextMenus.create({
    id:       "menu",
    title:    "ColourBlind",
    contexts: ["image"]
  },
  function() {
    [
      "Deuteranomaly",
      "Protanomaly",
      "Tritanomaly",
      "None"
    ].forEach(item => {
      chrome.contextMenus.create({
        id:       item,
        parentId: "menu",
        title:    item,
        contexts: ["image"]
      })
    })
  })
})
