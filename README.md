# ColourBlind

## What is it?

A lightweight and naive chrome extension which may prove useful to the
colourblind.

Adds a right click context menu item (ColourBlind) for images. Users can
choose to apply a filter for Deuteranomaly, Protanomaly or Tritanomaly. The
filter can be removed, and the original image restored, by choosing None.

## How does it work?

The extension works by adding an SVG to the page that you are visiting. The
SVG includes an feColorMatrix filter ..

[https://developer.mozilla.org/en-US/docs/Web/SVG/Element/feColorMatrix](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/feColorMatrix)

The extension also adds a custom style defintion (cbFilter) to the page that
references the feColorMatrix filter in the SVG.

When you choose to apply a colour correction, the feColorMatrix values are set
depending on the chosen correction and a class is added to the image so that
the filter is applied to it.

## Installation

If you haven't already done so, you need to enable Developer mode in Chrome.
Copy and paste this ..

    chrome://extensions

into Chrome's address bar. Enable Developer mode. To load an unpacked
extension, click the 'Load unpacked' button. 

To use this extension ..

Easy: git clone this repository and load the directory as an unpacked extension.

Less easy: download this repo as a zip file, unpack it somewhere and then load
the directory as an upacked extension.
