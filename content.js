let cbSVG       = document.createElementNS("http://www.w3.org/2000/svg", "svg"),
    cbFilter    = document.createElementNS("http://www.w3.org/2000/svg", "filter"),
    cbMatrix    = document.createElementNS("http://www.w3.org/2000/svg", "feColorMatrix"),
    cbStyle     = document.createElement("style"),
    lastClicked = false

cbSVG.setAttribute("style", "display:none")
cbFilter.setAttribute("id", "cbFilter")
cbMatrix.setAttribute("type", "matrix")

cbFilter.appendChild(cbMatrix)
cbSVG.appendChild(cbFilter)
document.body.appendChild(cbSVG)

cbStyle.innerHTML = `
.cbfilter {
  filter: url("#cbFilter");
}
`;
document.head.appendChild(cbStyle)

document.addEventListener("contextmenu", event => {
  lastClicked = event.target
})

chrome.runtime.onConnect.addListener(port => {
  port.name = "cbport"
  port.onMessage.addListener(msg => {

    if (msg.matrix === "None") {
      lastClicked.classList.remove("cbfilter")
      return
    }

    if (msg.matrix === "Deuteranomaly") {
      cbMatrix.setAttribute("values", "1 0 0 0 0 0.30 0.53 0.16 0 0 0.30 -0.30 1.06 0 0 0 0 0 1 0")
    }
    else if (msg.matrix === "Protanomaly") {
      cbMatrix.setAttribute("values", "1 0 0 0 0 0.73 0.17 0.10 0 0 0.34 -0.37 1.03 0 0 0 0 0 1 0")
    }
    else if (msg.matrix === "Tritanomaly") {
      cbMatrix.setAttribute("values", "0.74 -0.48 0.74 0 0 0.08 0.81 0.11 0 0 0 0 1 0 0 0 0 0 1 0")
    }

    lastClicked.classList.add("cbfilter")
  })
})
